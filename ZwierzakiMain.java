import java.util.ArrayList;
import java.util.List;

public class ZwierzakiMain {

    public static void main(String[] args) {


        Potrawa pot1 = new Potrawa();
        pot1.setNazwa(PotrawaNames.MIESO);
        pot1.setIlosc(10.2);


        ArrayList<Potrawa> psiePotrawy1 = new ArrayList<>();
        psiePotrawy1.add(pot1);

        Pies pies1 = new Pies("Puszek", psiePotrawy1);


        pies1.dajGlos();
        pies1.przedstawSie();
        System.out.println(pies1.getListaPotraw());

        System.out.println("---------------");

        Potrawa pot2 = new Potrawa(PotrawaNames.MLEKO, 5.5);
        Potrawa pot3 = new Potrawa(PotrawaNames.MIESO, 1.0);
        Potrawa pot4 = new Potrawa(PotrawaNames.GRYZON, 3.5);
        Potrawa pot5 = new Potrawa(PotrawaNames.OWAD, 40.5);
        Potrawa pot6 = new Potrawa(PotrawaNames.WARZYWA, 5.5);


        ArrayList<Potrawa> kociePotrawy1 = new ArrayList<>();
        kociePotrawy1.add(pot2);
        kociePotrawy1.add(pot3);
        kociePotrawy1.add(pot6);

        ArrayList<Potrawa> kociePotrawy2 = new ArrayList<>();
        kociePotrawy2.add(pot4);
        kociePotrawy2.add(pot5);


        ArrayList<Potrawa> kociePotrawy3 = new ArrayList<>();
        kociePotrawy3.add(pot2);
        kociePotrawy3.add(pot6);

        ArrayList<Potrawa> kociePotrawy4 = new ArrayList<>();
        kociePotrawy4.add(pot1);
        kociePotrawy4.add(pot3);

        ArrayList<Potrawa> kociePotrawy5 = new ArrayList<>();
        kociePotrawy5.add(pot2);
        kociePotrawy5.add(pot5);


        Kot kot1 = new Kot("Alik", kociePotrawy1, 10);
        Kot kot2 = new Kot("Mruczek", kociePotrawy2, 10);
        Kot kot3 = new Kot("Fafik", kociePotrawy3, 10);
        Kot kot4 = new Kot("Capik", kociePotrawy4, 10);
        Kot kot5 = new Kot("Filemon", kociePotrawy5, 10);
        Kot kot6 = new Kot("Kot Dominiki", kociePotrawy1, 10);

        kot1.dajGlos();
        kot1.zabijKota();
        kot1.przedstawSie();

        ArrayList<Zwierze> zwierzakiOsoby1 = new ArrayList<>();
        zwierzakiOsoby1.add(kot2);
        zwierzakiOsoby1.add(kot3);
        zwierzakiOsoby1.add(kot5);
        zwierzakiOsoby1.add(pies1);

        Potrawa zapasy1Osoby1 = new Potrawa(PotrawaNames.MIESO, 50);
        Potrawa zapasy2Osoby1 = new Potrawa(PotrawaNames.MLEKO, 100);
        Potrawa zapasy3Osoby1 = new Potrawa(PotrawaNames.KOSC, 50);
        Potrawa zapasy4Osoby1 = new Potrawa(PotrawaNames.WODA, 500);
        Potrawa zapasy5Osoby1 = new Potrawa(PotrawaNames.WARZYWA, 150);
        Potrawa zapasy6Osoby1 = new Potrawa(PotrawaNames.KARMA_DLA_KOTA, 50);
        Potrawa zapasy7Osoby1 = new Potrawa(PotrawaNames.KARMA_DLA_PSA, 50);
        Potrawa zapasy8Osoby1 = new Potrawa(PotrawaNames.KARMA_DLA_RYB, 50);
        Potrawa zapasy9Osoby1 = new Potrawa(PotrawaNames.OWAD, 30);
        Potrawa zapasy10Osoby1 = new Potrawa(PotrawaNames.GRYZON, 12);


        ArrayList<Potrawa> potrawyOsoby1 = new ArrayList<>();
        potrawyOsoby1.add(zapasy1Osoby1);
        potrawyOsoby1.add(zapasy2Osoby1);
        potrawyOsoby1.add(zapasy3Osoby1);
        potrawyOsoby1.add(zapasy4Osoby1);
        potrawyOsoby1.add(zapasy5Osoby1);
        potrawyOsoby1.add(zapasy6Osoby1);
        potrawyOsoby1.add(zapasy7Osoby1);
        potrawyOsoby1.add(zapasy8Osoby1);
        potrawyOsoby1.add(zapasy9Osoby1);
        potrawyOsoby1.add(zapasy10Osoby1);


        Czlowiek osoba1 = new Czlowiek("Karol", zwierzakiOsoby1, potrawyOsoby1);

        System.out.println(kot1.getListaPotraw());

        System.out.println("---------");
        System.out.println(osoba1);
        System.out.println("-------");
       // System.out.println(kot1.getListaPotraw());


        ArrayList<Potrawa> potrawaPusta = new ArrayList<>();

        potrawaPusta.add(pot2);

        Kot kotekTestowy = new Kot("Kotel",potrawaPusta,5);
        System.out.println("...........");
        System.out.println(kotekTestowy.getListaPotraw());

        Pies piesekTestowy = new Pies("Ciapek", potrawaPusta);
        System.out.println(kotekTestowy);

    }


}
