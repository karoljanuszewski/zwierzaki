import java.util.ArrayList;

public class Pies extends Zwierze{

    private Potrawa ulubionaPotrawaPsa = new Potrawa(PotrawaNames.KOSC,1);

    public Pies(String imie, ArrayList<Potrawa> potrawa) {
        super(imie, potrawa);
        dodajPotrawe(ulubionaPotrawaPsa);
    }






    @Override
    public void dajGlos() {
        System.out.println("hau!");
    }

    @Override
    public void przedstawSie() {
        System.out.println("nazywam sie "+getImie() );
    }

    @Override
    public String toString() {
        return String.format("pies %s , karmi sie %s",getImie(),getListaPotraw());
    }


}
