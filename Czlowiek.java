import java.util.ArrayList;

public class Czlowiek {

    String name;
    ArrayList<Zwierze> zwierzakiLista;
    ArrayList<Potrawa> potrawyLista;

    public Czlowiek(String name, ArrayList<Zwierze> zwierzakiLista, ArrayList<Potrawa> potrawyLista) {
        this.name = name;
        this.zwierzakiLista = zwierzakiLista;
        this.potrawyLista = potrawyLista;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Zwierze> getZwierzakiLista() {
        return zwierzakiLista;
    }

    public void setZwierzakiLista(ArrayList<Zwierze> zwierzakiLista) {
        this.zwierzakiLista = zwierzakiLista;
    }

    public ArrayList<Potrawa> getPotrawyLista() {
        return potrawyLista;
    }

    public void setPotrawyLista(ArrayList<Potrawa> potrawyLista) {
        this.potrawyLista = potrawyLista;

    }


    @Override
    public String toString() {
        return String.format("%s ma zwierzaki: %s , oraz dostepne potrawy: %s",getName(),getZwierzakiLista(),getPotrawyLista());
    }
}
