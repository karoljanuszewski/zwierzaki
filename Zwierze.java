import java.util.ArrayList;

public abstract class Zwierze {

    private String imie;
    private ArrayList<Potrawa> listaPotraw;



    public Zwierze(String imie, ArrayList<Potrawa> potrawa) {
        this.imie = imie;
        this.listaPotraw = potrawa;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public ArrayList<Potrawa> getListaPotraw() {
        return listaPotraw;
    }
    public void dodajPotrawe(Potrawa ulubionaPotrawa) {

        int index = listaPotraw.indexOf(ulubionaPotrawa);
        if (index==-1){
            listaPotraw.add(ulubionaPotrawa);
        }
        else {

            for (int i = 0; i <listaPotraw.size() ; i++) {

                if(listaPotraw.get(i).equals(ulubionaPotrawa)){

                    listaPotraw.get(i).setIlosc((ulubionaPotrawa.getIlosc())+(listaPotraw.get(i).getIlosc()));

                }
            }

        }


    }


    public void glodujZwierzaka(){



    }

    public void setListaPotraw(ArrayList<Potrawa> listaPotraw) {
        this.listaPotraw = listaPotraw;
    }

    public abstract void dajGlos();
    public abstract void przedstawSie();

    @Override
    public abstract String toString();
}
