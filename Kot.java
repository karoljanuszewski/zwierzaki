import java.util.ArrayList;


public class Kot extends Zwierze {


    private Potrawa ulubionaPotrawaKota = new Potrawa(PotrawaNames.MLEKO,5.0);

    private int iloscZyc;



    public Kot(String imie, ArrayList<Potrawa> potrawa, int iloscZyc) {
        super(imie, potrawa);

        dodajPotrawe(ulubionaPotrawaKota);
        this.iloscZyc = iloscZyc;

    }



    public int getIloscZyc() {
        return iloscZyc;
    }

    public void setIloscZyc(int iloscZyc) {
        this.iloscZyc = iloscZyc;
    }

    @Override
    public void dajGlos() {
        System.out.println("miau!");
    }

    @Override
    public void przedstawSie() {
        System.out.println("nazywam sie "+getImie()+" zostalo mi jeszcze "+getIloscZyc()+" zyc");


    }

    @Override
    public String toString() {
        return String.format("kot %s \n    karmi sie %s",getImie(),getListaPotraw());
    }

    public void zabijKota(){
        setIloscZyc(getIloscZyc()-1);
    }

}
