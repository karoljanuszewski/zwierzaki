
public class Potrawa {

    private PotrawaNames nazwa;
    private double ilosc;

    public Potrawa() {
    }

    public Potrawa(PotrawaNames nazwa, double ilosc){
        this.nazwa=nazwa;
        this.ilosc=ilosc;
    }

    public PotrawaNames getNazwa() {
        return nazwa;
    }

    public void setNazwa(PotrawaNames nazwa) {
        this.nazwa = nazwa;
    }

    public double getIlosc() {
        return ilosc;
    }

    public void setIlosc(double ilosc) {
        this.ilosc = ilosc;
    }

    @Override
    public String toString() {

        return String.format("karma: %s, w ilosci %f ", getNazwa(),getIlosc());
    }

    @Override
    public boolean equals(Object obj) {
        Potrawa pot = (Potrawa) obj;
        if(pot.getNazwa().equals(this.nazwa)){
            return true;
        }
        else {
            return false;
        }
    }
}
